package ru.chelnokov.task;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Класс, позволяющий протестировать работу Класса MultiTask
 *
 * @author Chelnokov E.I.
 */

public class MultiTaskTest {

    /**
     * метод, позволяющий протестировать метод умножения с двумя положительными числами
     */
    @Test
    public void positive() {
        int a = (int) (Math.random() * 100);
        int b = (int) (Math.random() * 100);
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }


    /**
     * метод, позволяющий протестировать метод умножения с двумя отрицательными числами
     */
    @Test
    public void negative() {
        int a = (int) (-100 + Math.random() * 100);
        int b = (int) (-100 + Math.random() * 100);
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }


    /**
     * метод, позволяющий протестировать метод умножения с одним положительным и одним отрицательным числами
     */
    @Test
    public void posneg() {
        int a = (int) (Math.random() * 100);
        int b = (int) (-100 + Math.random() * 100);
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }


    /**
     * метод, позволяющий протестировать метод умножения с двумя нулями
     */
    @Test
    public void zero() {
        int a = 0;
        int b = 0;
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }


    /**
     * метод, позволяющий протестировать метод умножения с нулём и положительным числом
     */
    @Test
    public void zeropos() {
        int a = 0;
        int b = (int) (Math.random() * 100);
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }


    /**
     * метод, позволяющий протестировать метод умножения с нулём и отрицательным числом
     */
    @Test
    public void zeroneg() {
        int a = 0;
        int b = (int) (-100 + Math.random() * 100);
        assertEquals(a * b, MultiTask.multiplication(a, b));
    }

}